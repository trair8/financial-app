--fill person table
insert into person (id, first_name, last_name) values (1, 'firstName1', 'lastName1');
insert into person (id, first_name, last_name) values (2, 'firstName2', 'lastName2');

--fill account table
insert into account (id, balance, person_id) values (1, 12.0041, 1);
insert into account (id, balance, person_id) values (2, 35.0677, 1);
insert into account (id, balance, person_id) values (3, 3555.0000, 2);