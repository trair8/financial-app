create table person (
id bigint not null auto_increment,
first_name varchar(50) not null,
last_name varchar(50) not null,
primary key(id)
);

create table account (
id bigint not null auto_increment,
balance decimal(10, 4) not null,
person_id bigint not null,
primary key(id),
foreign key(person_id) references person(id)
);