package com.codehoba.patter.controller

import com.codehoba.patter.PatterApplicationTests
import com.codehoba.patter.controller.dto.CreateAccountDto
import com.codehoba.patter.controller.dto.UpdateAccountBalanceDto
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.http.MediaType.APPLICATION_JSON
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors
import org.springframework.test.context.jdbc.Sql
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.*
import org.testcontainers.shaded.com.fasterxml.jackson.databind.ObjectMapper
import java.math.BigDecimal

@SpringBootTest
internal class AccountRestControllerTest : PatterApplicationTests() {

    @Autowired
    private lateinit var mockMvc: MockMvc

    @Test
    @Sql(*["/scripts/cleanTables.sql","/scripts/fillTables.sql"])
    fun getAccountTest() {
        mockMvc.perform(
            get("/account/{id}", 1)
                .accept(APPLICATION_JSON)
                .with(
                    SecurityMockMvcRequestPostProcessors.user("user").password("password")
                )
        )
            .andExpect(status().isOk)
            .andExpect(content().contentType(APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value("1"))
            .andExpect(jsonPath("$.balance").value("12.0041"))
            .andExpect(jsonPath("$.personId").value("1"))
    }

    @Test
    @Sql(*["/scripts/cleanTables.sql","/scripts/fillTables.sql"])
    fun getAccountTest_404() {
        mockMvc.perform(
            get("/account/{id}", 67)
                .accept(APPLICATION_JSON)
                .with(
                    SecurityMockMvcRequestPostProcessors.user("user").password("password")
                )
        )
            .andExpect(status().isNotFound)
    }

    @Test
    fun getAccountTest_401() {
        mockMvc.perform(
            get("/account/{id}", 1)
                .accept(APPLICATION_JSON)
        )
            .andExpect(status().isUnauthorized)
    }

    @Test
    @Sql(*["/scripts/cleanTables.sql","/scripts/fillTables.sql"])
    fun findAllAccountsTest() {
        mockMvc.perform(
            get("/account")
                .param("personId", "1")
                .accept(MediaType.APPLICATION_JSON)
                .with(
                    SecurityMockMvcRequestPostProcessors.user("user").password("password")
                )
        )
            .andExpect(status().isOk)
            .andExpect(content().contentType(APPLICATION_JSON))
            .andExpect(jsonPath("$.length()").value(2))
            .andExpect(content().string("[{\"id\":1,\"balance\":12.0041,\"personId\":1},{\"id\":2,\"balance\":35.0677,\"personId\":1}]"))
    }

    @Test
    fun createAccountTest() {
        val mapper = ObjectMapper()
        mockMvc.perform(
            post("/account")
                .contentType(APPLICATION_JSON)
                .content(mapper.writeValueAsString(CreateAccountDto(1)))
                .accept(APPLICATION_JSON)
                .with(
                    SecurityMockMvcRequestPostProcessors.user("user").password("password")
                )
        )
            .andExpect(status().isOk)
            .andExpect(content().contentType(APPLICATION_JSON))
            .andExpect(jsonPath("$.balance").value("0"))
            .andExpect(jsonPath("$.personId").value("1"))
    }

    @Test
    @Sql(*["/scripts/cleanTables.sql","/scripts/fillTables.sql"])
    fun deleteAccountTest() {
        mockMvc.perform(
            delete("/account/{id}", 1)
                .accept(APPLICATION_JSON)
                .with(
                    SecurityMockMvcRequestPostProcessors.user("user").password("password")
                )
        )
            .andExpect(status().isOk)
            .andExpect(content().contentType(APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value("1"))
            .andExpect(jsonPath("$.balance").value("12.0041"))
            .andExpect(jsonPath("$.personId").value("1"))
    }

    @Test
    @Sql(*["/scripts/cleanTables.sql","/scripts/fillTables.sql"])
    fun updateAccountBalanceTest() {
        val mapper = ObjectMapper()
        mockMvc.perform(
            put("/account")
                .contentType(APPLICATION_JSON)
                .content(mapper.writeValueAsString(UpdateAccountBalanceDto(1, BigDecimal.valueOf(50.23))))
                .accept(APPLICATION_JSON)
                .with(
                    SecurityMockMvcRequestPostProcessors.user("user").password("password")
                )
        )
            .andExpect(status().isOk)
            .andExpect(content().contentType(APPLICATION_JSON))
            .andExpect(jsonPath("$.balance").value("50.23"))
            .andExpect(jsonPath("$.personId").value("1"))
    }

}