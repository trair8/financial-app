package com.codehoba.patter

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class PatterApplication

fun main(args: Array<String>) {
	runApplication<PatterApplication>(*args)
}
