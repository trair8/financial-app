package com.codehoba.patter.repository

import com.codehoba.patter.model.Account
import com.codehoba.patter.model.Person
import org.springframework.data.repository.CrudRepository

interface AccountRepository : CrudRepository<Account, Long> {
    fun findAllByPersonId(personId: Long): List<Account>
    fun deleteAllByPersonId(personId: Long): List<Account>
}

interface PersonRepository : CrudRepository<Person, Long>