package com.codehoba.patter.model

import javax.persistence.*

@Entity
@Table
class Person(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long? = null,
    @Column(name = "firstName", nullable = false)
    var firstName: String,
    @Column(nullable = false)
    var lastName: String,
)