package com.codehoba.patter.service

import com.codehoba.patter.model.Person
import com.codehoba.patter.repository.AccountRepository
import com.codehoba.patter.repository.PersonRepository
import org.springframework.stereotype.Service
import javax.transaction.Transactional

@Service
@Transactional
class PersonServiceImpl (
    private val personRepository: PersonRepository,
    private val accountRepository: AccountRepository
) : PersonService {
    override fun findAllPersons(): List<Person> = personRepository.findAll().toList()

    override fun getPerson(id: Long): Person = personRepository.findById(id)
        .orElseThrow { NoSuchElementException(String.format("Person is not exist. id = %s", id)) }

    override fun deletePerson(id: Long): Person {
        val person = getPerson(id)
        accountRepository.deleteAllByPersonId(id)
        personRepository.delete(person)
        return person
    }

    override fun createPerson(firstName: String, lastName: String): Person =
        personRepository.save(Person(firstName = firstName, lastName = lastName))
}