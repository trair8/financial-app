package com.codehoba.patter.service

import com.codehoba.patter.model.Account
import com.codehoba.patter.model.Person
import java.math.BigDecimal

interface AccountService {
    fun findAllAccounts(personId: Long): List<Account>
    fun getAccount(id: Long): Account
    fun deleteAccount(id: Long): Account
    fun createAccount(personId: Long): Account

    fun updateAccountBalance(id: Long, balance: BigDecimal): Account

}

interface PersonService {
    fun findAllPersons(): List<Person>
    fun getPerson(id: Long): Person
    fun deletePerson(id: Long): Person
    fun createPerson(firstName: String, lastName: String): Person

}