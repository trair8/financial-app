package com.codehoba.patter.service

import com.codehoba.patter.model.Account
import com.codehoba.patter.repository.AccountRepository
import org.springframework.stereotype.Service
import java.math.BigDecimal
import javax.transaction.Transactional

@Service
@Transactional
class AccountServiceImpl(
    private val accountRepository: AccountRepository,
    private val personRepository: AccountRepository
) : AccountService {
    override fun findAllAccounts(personId: Long): List<Account> = accountRepository.findAllByPersonId(personId)

    override fun getAccount(id: Long): Account = accountRepository.findById(id)
        .orElseThrow { NoSuchElementException(String.format("Account is not exist. id = %s", id)) }

    override fun deleteAccount(id: Long): Account {
        val account = getAccount(id)
        accountRepository.delete(account)
        return account
    }

    override fun createAccount(personId: Long): Account {
        personRepository.findById(personId)
            .orElseThrow { NoSuchElementException(String.format("Person is not exist. id = %s", personId)) }
        return accountRepository.save(Account(balance = BigDecimal.ZERO, personId = personId))
    }

    override fun updateAccountBalance(id: Long, balance: BigDecimal): Account {
        val account = accountRepository.findById(id)
            .orElseThrow { NoSuchElementException(String.format("Account is not exist. id = %s", id)) }
        account.balance = balance
        return accountRepository.save(account)
    }
}