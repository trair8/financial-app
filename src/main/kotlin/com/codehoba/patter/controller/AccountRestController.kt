package com.codehoba.patter.controller

import com.codehoba.patter.controller.dto.CreateAccountDto
import com.codehoba.patter.controller.dto.UpdateAccountBalanceDto
import com.codehoba.patter.model.Account
import com.codehoba.patter.service.AccountService
import io.swagger.annotations.Api
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.Parameter
import io.swagger.v3.oas.annotations.media.ArraySchema
import io.swagger.v3.oas.annotations.media.Content
import io.swagger.v3.oas.annotations.media.Schema
import io.swagger.v3.oas.annotations.responses.ApiResponse
import io.swagger.v3.oas.annotations.responses.ApiResponses
import org.springframework.web.bind.annotation.*

@Api("API for operations with accounts")
@RestController
@RequestMapping("/account")
class AccountRestController(
    private val accountService: AccountService
) {
    @Operation(summary = "Get all Account by PersonId")
    @ApiResponses(value = [
        ApiResponse(responseCode = "200", description = "Found Accounts", content = [
            (Content(mediaType = "application/json", array = (
                    ArraySchema(schema = Schema(implementation = List::class)))))])]
    )
    @GetMapping
    fun findAll(@Parameter(description = "Id of Person") @RequestParam personId: Long) =
        accountService.findAllAccounts(personId)

    @Operation(summary = "Get an Account by id")
    @ApiResponses(value = [
        ApiResponse(responseCode = "200", description = "Found an Account", content = [
            (Content(mediaType = "application/json", array = (
                    ArraySchema(schema = Schema(implementation = Account::class)))))]),
        ApiResponse(responseCode = "404", description = "Did not find an Account", content = [Content()])]
    )
    @GetMapping("/{id}")
    fun getAccount(@Parameter(description = "Id of Account") @PathVariable id: Long) = accountService.getAccount(id)

    @Operation(summary = "Create an Account")
    @ApiResponses(value = [
        ApiResponse(responseCode = "200", description = "Created an Account", content = [
            (Content(mediaType = "application/json", array = (
                    ArraySchema(schema = Schema(implementation = Account::class)))))]),
        ApiResponse(responseCode = "400", description = "Bad request", content = [Content()]),
        ApiResponse(responseCode = "404", description = "Did not find a Person", content = [Content()])]
    )
    @PostMapping
    fun createAccount(@RequestBody dto: CreateAccountDto) = accountService.createAccount(dto.personId)

    @Operation(summary = "Delete an Account by id")
    @ApiResponses(value = [
        ApiResponse(responseCode = "200", description = "Deleted an Account", content = [
            (Content(mediaType = "application/json", array = (
                    ArraySchema(schema = Schema(implementation = Account::class)))))]),
        ApiResponse(responseCode = "404", description = "Did not find an Account", content = [Content()])]
    )
    @DeleteMapping("/{id}")
    fun deleteAccount(@Parameter(description = "Id of Account") @PathVariable id: Long) =
        accountService.deleteAccount(id)

    @Operation(summary = "Update an Account Balance")
    @ApiResponses(value = [
        ApiResponse(responseCode = "200", description = "Updated an Account Balance", content = [
            (Content(mediaType = "application/json", array = (
                    ArraySchema(schema = Schema(implementation = Account::class)))))]),
        ApiResponse(responseCode = "400", description = "Bad request", content = [Content()]),
        ApiResponse(responseCode = "404", description = "Did not find an Account", content = [Content()])]
    )
    @PutMapping
    fun updateAccountBalance(@RequestBody dto: UpdateAccountBalanceDto) =
        accountService.updateAccountBalance(dto.id, dto.balance)

}