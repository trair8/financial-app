package com.codehoba.patter.controller

import com.codehoba.patter.controller.dto.CreateAccountDto
import com.codehoba.patter.controller.dto.CreatePersonDto
import com.codehoba.patter.service.PersonService
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/person")
class PersonRestController (
    private val personService: PersonService
        ) {
    @GetMapping
    fun findAll() = personService.findAllPersons()

    @GetMapping("/{id}")
    fun getAccount(@PathVariable id: Long) = personService.getPerson(id)

    @PostMapping
    fun createAccount(@RequestBody dto: CreatePersonDto) = personService.createPerson(dto.firstName, dto.lastName)

    @DeleteMapping("/{id}")
    fun deleteAccount(@PathVariable id: Long) = personService.deletePerson(id)

}