package com.codehoba.patter.controller.dto

import java.math.BigDecimal

data class CreateAccountDto(
    val personId: Long
)

data class UpdateAccountBalanceDto(
    val id: Long,
    val balance: BigDecimal
)