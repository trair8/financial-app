package com.codehoba.patter.controller.dto

data class CreatePersonDto(
    val firstName: String,
    val lastName: String
)