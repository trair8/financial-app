# Financial App

It's an example of code for freelance project

## Features
- CRUD Operation for Account (and Person)
## Tech Stack
- Kotlin
- Spring Boot
- Spring Security (Basic as example)
- Spring Data Jpa
- SpringFox Swagger
- Flyway
- Testcontainers(Mysql) for integration tests(JUnit 5)

### Local environment development

1) Run "docker-compose up" in root of project to start MySQL
2) Run App using local profile: -Dspring.profiles.active=local
